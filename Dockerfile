FROM ruby:3.1
LABEL org.opencontainers.image.authors="Ettore Leandro Tognoli <ettoreleandrotognoli@gmail.com>"
RUN apt-get update && apt-get install -y \
  default-jdk-headless \
  graphviz \
  && rm -rf /var/lib/apt/lists/*
COPY --from=plantuml/plantuml /opt/plantuml.jar /opt/plantuml.jar
RUN echo '#!/bin/bash \n\
\n\
java -jar /opt/plantuml.jar "$@"'> /usr/local/bin/plantuml
RUN chmod +x /usr/local/bin/plantuml
