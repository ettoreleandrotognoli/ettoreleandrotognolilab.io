---
layout: page
title: Quem sou eu?
permalink: /about/
---

Olá! Sou um engenheiro de software, professor universitário e entusiasta de projetos criativos. Este blog é meu espaço para compartilhar ideias, experiências e experimentos nas áreas que mais me fascinam: música, tecnologia e projetos DIY.

Tenho formação como mestre em Ciência da Computação pela UFSCar e atualmente estou cursando bacharelado em música na UNIS. Esse equilíbrio entre a lógica da computação e a arte da música é o que guia muitos dos projetos que desenvolvo e compartilho por aqui.

Sou apaixonado por boas práticas de programação, como o conceito de Clean Code, e por explorar ferramentas como impressoras 3D, routers CNC e componentes eletrônicos para criar projetos divertidos e que refletem meus interesses pessoais. Adoro unir criatividade e funcionalidade, transformando ideias em realidade.

Se você gosta de aprender algo novo, explorar ideias criativas e acompanhar projetos que conectam tecnologia e arte, este blog foi feito para você!
